﻿using Newtonsoft.Json;
using Reflection;
using Xunit;

namespace ReflectionTests
{
    public class SerializerTests
    {
        [Fact]
        public void SerializeFromObjectToCSV_ByNewtonsoftJson()
        {
            var foo = new Foo();
            var expected = foo.Get();
            var settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Objects;

            string serialized = JsonConvert.SerializeObject(expected, settings);
            var deserialized = JsonConvert.DeserializeObject(serialized, settings);
            var actual = (Foo)deserialized;

            Assert.Equal(expected.i1, actual.i1);
            Assert.Equal(expected.i2, actual.i2);
            Assert.Equal(expected.i3, actual.i3);
            Assert.Equal(expected.i4, actual.i4);
            Assert.Equal(expected.i5, actual.i5);
        }

        [Fact]
        public void SerializeFromObjectToCSV_ShouldWork()
        {
            var foo = new Foo();
            var expected = foo.Get();
            
            var csv = Serializer.SerializeFromObjectToCSV(expected);
            var deserialized = Serializer.DeserializeFromCSVToObject(csv);
            var actual = (Foo)deserialized;

            Assert.Equal(expected.i1, actual.i1);
            Assert.Equal(expected.i2, actual.i2);
            Assert.Equal(expected.i3, actual.i3);
            Assert.Equal(expected.i4, actual.i4);
            Assert.Equal(expected.i5, actual.i5);
        }
    }

    public class Foo
    {
        public int i1 { get; set; }
        public int i2 { get; set; }
        public int i3 { get; set; }
        public int i4 { get; set; }
        public int i5 { get; set; }
        public Foo Get()
        {
            return new Foo { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
        }
    }
}
