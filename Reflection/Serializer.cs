﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Reflection
{
    /// <summary> Serializer </summary>
    public static class Serializer
    {
        private const string Separator = ";";

        /// <summary> Serialize from object to CSV </summary>
        /// <param name="obj">any object</param>
        /// <returns>CSV</returns>
        public static string SerializeFromObjectToCSV(object obj)
        {
            StringBuilder sb = new StringBuilder();

            Type type = obj.GetType();
            sb.Append($"{type.AssemblyQualifiedName}");
            sb.Append(Separator);

            PropertyInfo[] props = type.GetProperties();
            foreach (var prop in props)
            {
                sb.Append(prop.Name);
                sb.Append(":");
                sb.Append(prop.GetValue(obj));
                sb.Append(",");
            }
            return sb.ToString().TrimEnd(',');
        }

        /// <summary> Deserialize from CSV to object</summary>
        /// <param name="csv">string in CSV format</param>
        /// <returns>object</returns>
        public static object DeserializeFromCSVToObject(string csv)
        {
            var fields = csv.Split(Separator);
            if(fields.Length == 0) return null;

            var typeName = fields.First();
            Type type = Type.GetType(typeName, false, true);
            PropertyInfo[] props = type.GetProperties();

            Dictionary<string, dynamic> map = new Dictionary<string, dynamic>();
            foreach (var field in fields[1].Split(','))
            {
                var kv = field.Split(':');
                map.Add(kv[0], kv[1]);
            }

            var obj = Activator.CreateInstance(type);
            foreach (var prop in props)
            {
                if(map.ContainsKey(prop.Name))
                {
                    prop.SetValue(obj, Convert.ChangeType(map[prop.Name], prop.PropertyType));
                }
            }
            return obj;
        }
    }
}