﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Order;
using Newtonsoft.Json;
using System.ComponentModel;

namespace Reflection
{
    [MemoryDiagnoser]
    [Orderer(SummaryOrderPolicy.FastestToSlowest)]
    [RankColumn]
    public class SerializerBenchmarks
    {
        private static readonly Foo _foo = new Foo();

        [Benchmark(Baseline = true)]
        public void SerializeFromObjectToCSV_Newtonsoft()
        {
            string serialized = JsonConvert.SerializeObject(_foo);
        }
        [Benchmark]
        public void SerializeFromObjectToCSV_My()
        {
            string serialized = Serializer.SerializeFromObjectToCSV(_foo);
        }

        [Benchmark]
        public void Serialize_Deserialize_Newtonsoft()
        {
            string serialized = JsonConvert.SerializeObject(_foo);
            var obj = JsonConvert.DeserializeObject(serialized);
        }

        [Benchmark]
        public void Serialize_Deserialize_My()
        {
            string serialized = Serializer.SerializeFromObjectToCSV(_foo);
            var obj = Serializer.DeserializeFromCSVToObject(serialized);
        }
    }

    public class Foo
    {
        public int i1 { get; set; }
        public int i2 { get; set; }
        public int i3 { get; set; }
        public int i4 { get; set; }
        public int i5 { get; set; }
        public Foo()
        {
            i1 = 1;
            i2 = 2;
            i3 = 3;
            i4 = 4;
            i5 = 5; 
        }
    }
}
